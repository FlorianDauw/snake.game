﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Snake.app
{
    class Program
    {
        static void Main(string[] args)
        {
            #region Déclaration des variables

            ConsoleKeyInfo touche = new ConsoleKeyInfo();
            List<string> snake = new List<string>();
            snake.Add("■");

            List<int> posSnakeX = new List<int>();
            List<int> posSnakeY = new List<int>();

            int positionX = 31;
            int positionY = 15;

            posSnakeX.Add(positionX);
            posSnakeY.Add(positionY);

            int[] positions = new int[2];
            positions[0] = positionX;
            positions[1] = positionY;

            int directionX = 0;
            int directionY = 0;
            int direction = 0;
            bool jeu = true;
            bool gameover = false;

            bool pomme = false;

            int[] positionPomme = new int[2];

            int score = 0;
            int difficulte;
            int vitesse;

            #endregion

            #region Fonctions

            #region Fonction Génératrice de nombre aléatoire

            int RandomNumber(int min, int max)
            {
                Random random = new Random();
                return random.Next(min, max);
            }

            #endregion

            #region Fonction Génératrice de position (X,Y) pour la pomme

            int[] AfficherPomme(int[] positionpomme)
            {
                positionpomme[0] = RandomNumber(3, 60);

                if (positionpomme[0] % 2 == 0)
                    positionpomme[0]++;

                positionpomme[1] = RandomNumber(4, 30);

                return positionpomme;
            }

            #endregion

            #region Fonction Dessinant la zone de jeu

            void Dessin(int points)
            {
                Console.BackgroundColor = ConsoleColor.White;
                Console.ForegroundColor = ConsoleColor.Black;
                Console.Write($"~~~~~~~~~~~~~~~~~~~~~");
                Console.Write($"\tSCORE: { points}\t");
                Console.Write($"~~~~~~~~~~~~~~~~~~~~~~\n");

                Console.WriteLine("┌------------------------------------------------------------┐");
                Console.WriteLine("|                                                            |");
                Console.WriteLine("|                                                            |");
                Console.WriteLine("|                                                            |");
                Console.WriteLine("|                                                            |");
                Console.WriteLine("|                                                            |");
                Console.WriteLine("|                                                            |");
                Console.WriteLine("|                                                            |");
                Console.WriteLine("|                                                            |");
                Console.WriteLine("|                                                            |");
                Console.WriteLine("|                                                            |");
                Console.WriteLine("|                                                            |");
                Console.WriteLine("|                                                            |");
                Console.WriteLine("|                                                            |");
                Console.WriteLine("|                                                            |");
                Console.WriteLine("|                                                            |");
                Console.WriteLine("|                                                            |");
                Console.WriteLine("|                                                            |");
                Console.WriteLine("|                                                            |");
                Console.WriteLine("|                                                            |");
                Console.WriteLine("|                                                            |");
                Console.WriteLine("|                                                            |");
                Console.WriteLine("|                                                            |");
                Console.WriteLine("|                                                            |");
                Console.WriteLine("|                                                            |");
                Console.WriteLine("|                                                            |");
                Console.WriteLine("|                                                            |");
                Console.WriteLine("|                                                            |");
                Console.WriteLine("|                                                            |");
                Console.WriteLine("|                                                            |");
                Console.WriteLine("└------------------------------------------------------------┘");
            }

            #endregion

            #region Fonction affichage du Game Over

            void gameOver(int points)
            {

                gameover = true;

                System.Threading.Thread.Sleep(1000);

                Console.Clear();

                Console.BackgroundColor = ConsoleColor.DarkRed;
                Console.ForegroundColor = ConsoleColor.White;

                Console.WriteLine(@"                                                           ");
                Console.WriteLine(@"                                                           ");
                Console.WriteLine(@"                                                           ");
                Console.WriteLine(@"                                                           ");
                Console.WriteLine(@"              _____             _                          ");
                Console.WriteLine(@"              |  __ \           | |                        ");
                Console.WriteLine(@"              | |__) |__ _ __ __| |_   _                   ");
                Console.WriteLine(@"              |  ___/ _ \ '__/ _` | | | |                  ");
                Console.WriteLine(@"              | |  |  __/ | | (_| | |_| |_ _ _             ");
                Console.WriteLine(@"              |_|   \___|_|  \__,_|\__,_(_|_|_)            ");
                Console.WriteLine(@"                                                           ");
                Console.WriteLine(@"                                                           ");
                Console.WriteLine(@"                                                           ");
                Console.WriteLine(@"                                                           ");
                Console.WriteLine($"                          Score : {points}\t\t\t   ");
                Console.WriteLine(@"     Appuyez sur n'importe quelle touche pour quitter!     ");

                Console.ReadKey();
                //return gameover;
            }

            #endregion

            #endregion

            #region Fichier meilleurs scores
            string fileName = "scores.txt";
            List<string> meilleursScores = new List<string>();
            List<int> bests = new List<int>();
            if (!File.Exists(fileName))
            {
                FileStream fs = File.Create(fileName);
                fs.Close();
            }
            else
            {
                string[] tab = File.ReadAllLines(fileName);
                foreach(string v in tab)
                {
                    bests.Add(int.Parse(v)) ;
                }
                meilleursScores = tab.ToList();
                bests.Sort();
                bests.Reverse();
            }
            #endregion

            #region Ecran d'accueil
            Console.BackgroundColor = ConsoleColor.DarkGreen;
            Console.ForegroundColor = ConsoleColor.White;

            Console.WriteLine(@"******************************************************");
            Console.WriteLine(@"*              _____             _                   *");
            Console.WriteLine(@"*             / ____|           | |                  *");
            Console.WriteLine(@"*            | (___  _ __   __ _| | _____            *");
            Console.WriteLine(@"*             \___ \| '_ \ / _` | |/ / _ \           *");
            Console.WriteLine(@"*             ____) | | | | (_| |   <  __/           *");
            Console.WriteLine(@"*            |_____/|_| |_|\__,_|_|\_\___|           *");
            Console.WriteLine(@"*                                                    *");
            Console.WriteLine(@"******************************************************");
            Console.WriteLine(@"* Meilleurs scores :                                 *");
            if (meilleursScores.Count() > 0)
            {
                if (meilleursScores.Count() == 1)
                    Console.WriteLine($"* 1.\t{bests[0]}\t\t\t\t\t     *");
                else if(meilleursScores.Count == 2)
                {
                    Console.WriteLine($"* 1.\t{bests[0]}\t\t\t\t\t     *");
                    Console.WriteLine($"* 2.\t{bests[1]}\t\t\t\t\t     *");
                }
                else
                {
                    for (int i = 0; i < 3; i++)
                    {
                        Console.WriteLine($"* {i + 1}.\t{bests[i]}\t\t\t\t\t     *");
                    }
                } 
            }
            else Console.WriteLine("* Aucun score enregistré                              ");
            Console.WriteLine(@"******************************************************");
            Console.WriteLine(@"* Difficulté :                                       *");
            Console.WriteLine(@"* 1) Facile      ====>    1 point par pomme mangée.  *");
            Console.WriteLine(@"* 2) Moyen       ====>    3 points par pomme mangée. *");
            Console.WriteLine(@"* 3) Difficile   ====>    5 points par pomme mangée. *");
            Console.WriteLine(@"* 4) Evolutive   ====>    points évolutifs.          *");
            Console.WriteLine(@"******************************************************");

            Console.BackgroundColor = ConsoleColor.Black;
            Console.ForegroundColor = ConsoleColor.White;

            Console.WriteLine("Choisissez un mode de jeu (1/2/3/4) : ");
            do
            {
                while ((!int.TryParse(Console.ReadLine(), out difficulte)))
                {
                    Console.WriteLine("Choisissez un mode de jeu (1/2/3/4) : ");
                }
            } while ((difficulte < 1) || (difficulte > 4));

            switch (difficulte)
            {
                case 1:
                    vitesse = 150;
                    break;
                case 2:
                    vitesse = 100;
                    break;
                case 3:
                    vitesse = 50;
                    break;
                case 4:
                    vitesse = 150;
                    break;
                default:
                    vitesse = 100;
                    break;
            }

            #endregion

            while (jeu)
            {
                Console.SetCursorPosition(0, 0);
                Dessin(score);

                positionX += directionX;
                positionY += directionY;

                #region Enregistrement de la position (X,Y) du snake

                if ((positions[0] != positionX) || positions[1] != positionY)
                {
                    if (positionX < 0) positionX = 0;
                    posSnakeX.Add(positionX);
                    posSnakeY.Add(positionY);

                    positions[0] = positionX;
                    positions[1] = positionY;
                }

                #endregion

                #region Positionnement et affichage de la pomme

                if (pomme == false)
                {
                    positionPomme = AfficherPomme(positionPomme);
                    pomme = true;
                }

                Console.SetCursorPosition(positionPomme[0], positionPomme[1]);
                Console.ForegroundColor = ConsoleColor.Red;
                Console.Write("*");
                Console.ForegroundColor = ConsoleColor.DarkGreen;


                #endregion

                #region Calcul et affichage du snake

                for (int i = 0; i < snake.Count(); i++)
                {
                    Console.SetCursorPosition(posSnakeX[posSnakeX.Count - 1 - i], posSnakeY[posSnakeY.Count - 1 - i]);
                    Console.Write("■");
                }

                #endregion

                #region Détection de la touche appuyée

                if (Console.KeyAvailable)
                {
                    touche = Console.ReadKey(true);
                    direction = (int)touche.Key;
                }

                #endregion

                #region Changement de direction selon la touche appuyée

                //HAUT
                if (direction == 38 && directionY != 1)
                {
                    directionX = 0;
                    directionY = -1;
                }

                //BAS
                else if (direction == 40 && directionY != -1)
                {
                    directionX = 0;
                    directionY = 1;
                }

                //GAUCHE
                else if (direction == 37 && directionX != 2)
                {
                    directionX = -2;
                    directionY = 0;
                }

                //DROITE
                else if (direction == 39 && directionX != -2)
                {
                    directionX = 2;
                    directionY = 0;
                }

                #endregion

                #region Détection de collision avec la queue du serpent
                for (int i = 1; i < snake.Count(); i++)
                {
                    if ((posSnakeX[posSnakeX.Count - 1 - i] == positions[0]) &&
                        (posSnakeY[posSnakeX.Count - 1 - i] == positions[1]))
                    {
                        meilleursScores.Add(score.ToString());
                        File.WriteAllLines(fileName, meilleursScores);
                        gameOver(score);
                    }
                }
                #endregion

                #region Détection de collision avec les murs

                if (positionX < 1 || positionX > 60 || positionY < 2 || positionY > 30)
                {
                    meilleursScores.Add(score.ToString());
                    File.WriteAllLines(fileName, meilleursScores);
                    gameOver(score);
                }

                if (gameover)
                {
                    break;
                }

                #endregion

                #region Détection de collision avec la pomme

                //Collision avec la pomme

                if (positionPomme[0] == positionX && positionPomme[1] == positionY)
                {
                    snake.Add("■");
                    pomme = false;
                    switch (difficulte)
                    {
                        case 1:
                            score++;
                            break;
                        case 2:
                            score += 3;
                            break;
                        case 3:
                            score += 5;
                            break;
                        case 4:
                            if (vitesse <= 125)
                                if (vitesse <= 100)
                                    if (vitesse <= 75)
                                        if (vitesse <= 50)
                                            if (vitesse <= 30)
                                                score += 6;
                                            else score += 5;
                                        else score += 4;
                                    else score += 3;
                                else score += 2;
                            else score += 1;
                            if(vitesse > 20)
                              vitesse -= 5;
                            break;
                        default:
                            score++;
                            break;
                    }
                }

                #endregion

                Console.SetCursorPosition(0, 0);
                System.Threading.Thread.Sleep(vitesse);
            }
        }
    }
}